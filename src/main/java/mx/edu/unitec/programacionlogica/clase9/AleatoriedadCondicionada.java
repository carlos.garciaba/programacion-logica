package mx.edu.unitec.programacionlogica.clase9;

/**
 *
 * @author Carlos GB
 */
class AleatoriedadCondicionada {

    double c = Math.atan(10);
    double entrada, salida;

    public double getSalida(double nNeuro, double num) {
        entrada = num * 100 / nNeuro;
        salida = (Math.atan((entrada / 10) - 10) * 100 / c) + 100;
        salida = salida * nNeuro / 100;
        return salida;
    }

}
