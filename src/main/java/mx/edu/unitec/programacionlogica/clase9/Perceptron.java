package mx.edu.unitec.programacionlogica.clase9;

import java.util.Random;
import javax.swing.JOptionPane;
import mx.edu.unitec.programacionlogica.clase8.Neurona;

/**
 *
 * @author Carlos GB
 */
public class Perceptron {
    
    public static void main(String[] args) {
        
        //Valores Entradas
        double x1=1.4;
        double x2=-0.33;
        
        //Valores Pesos (aleatorios)
        double w1=new Random().nextDouble();
        double w2=new Random().nextDouble();
        
        Neurona n=new Neurona(x1, x2, w1, w2);
        
        JOptionPane.showMessageDialog(null, "Entrada 1 (x1): "+x1);
        JOptionPane.showMessageDialog(null, "Entrada 2 (x2): "+x2);
        JOptionPane.showMessageDialog(null, "Salida 1 (y1): "+n.getY1());
        
    }

}
