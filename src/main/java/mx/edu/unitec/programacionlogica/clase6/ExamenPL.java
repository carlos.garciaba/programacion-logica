package mx.edu.unitec.programacionlogica.clase6;

import java.util.Random;
import javax.swing.JOptionPane;

/**
 *
 * @author Carlos GB
 */
public class ExamenPL {

    public static void main(String[] args) {
        String pregunta,respuesta,mensaje;
        
        int item,totalPreguntas,cuentaPreguntas=1,totalAciertos=0;
        
        String preguntas[]={"Variable","Atomo","Regla"};
        String respuestas[]={"Valor cambia","functor","Relaciones"};
        
        String total=JOptionPane.showInputDialog("Bienvenido a examen PL\n"
                + "¿Cuantas preguntas quieres responder ?");
        
        totalPreguntas=Integer.parseInt(total);
        while(cuentaPreguntas<=totalPreguntas){
            item=new Random().nextInt(preguntas.length);
            pregunta=String.format("Escribe palabra que se identifica a la PL %s", preguntas[item]);
            respuesta=JOptionPane.showInputDialog(null,"respuesta de PL",JOptionPane.QUESTION_MESSAGE);
            if(!respuestas[item].equals(respuesta)){
                JOptionPane.showMessageDialog(null, "Incorrecto","Respuesta de PL",  JOptionPane.ERROR_MESSAGE);
            }else{
                JOptionPane.showMessageDialog(null, "Correcto","Respuesta de PL",  JOptionPane.INFORMATION_MESSAGE);            
                totalAciertos++;
            }
            cuentaPreguntas++;
        }
        mensaje=String.format("De %d preguntas obtuviste %d correctas", totalPreguntas,totalAciertos);
        JOptionPane.showMessageDialog(null, mensaje+"\n\n Autor: Carlos Garcia");
    }
    
}
