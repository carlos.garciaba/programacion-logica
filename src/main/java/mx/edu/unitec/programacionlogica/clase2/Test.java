package mx.edu.unitec.programacionlogica.clase2;

import javax.swing.JOptionPane;

/**
 *
 * @author Carlos GB
 */
public class Test extends Contenedora{
    
    public static void main(String[] args) {
        int menu;
        Test ap=new Test();
        do{
            String ventMenu=JOptionPane.showInputDialog("Bienvenido a calculadora\n"
                    + "1.- Suma \n 2.- Resta \n3.- Division \n4.- Multiplicacion\n"
                    + "5.- Salir");
            
            menu=Integer.parseInt(ventMenu);
            switch(menu){
                case 1:
                    ap.sumar();
                    break;
                case 2:
                    ap.resta();
                    break;
                case 3:
                    ap.divi();
                    break;
                case 4:
                    ap.multi();
                    break;
                case 5:
                    JOptionPane.showMessageDialog(null, "Finalizo programa");
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Opcion invalida");
                    break;
            }
            
        }while(menu!=5);
    }
    
}
