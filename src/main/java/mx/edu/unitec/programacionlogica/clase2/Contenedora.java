package mx.edu.unitec.programacionlogica.clase2;

import javax.swing.JOptionPane;

/**
 *
 * @author Carlos GB
 */
public class Contenedora {
    
    
    public void sumar(){
        String ventNum1=JOptionPane.showInputDialog("Digita numero 1");
        int num1=Integer.parseInt(ventNum1);
        String ventNum2=JOptionPane.showInputDialog("Digita numero 2");
        int num2=Integer.parseInt(ventNum2);
        int rs=num1+num2;
        JOptionPane.showMessageDialog(null, "La suma es: \n "+rs);
    }
    public void resta(){
        String ventNum1=JOptionPane.showInputDialog("Digita numero 1");
        int num1=Integer.parseInt(ventNum1);
        String ventNum2=JOptionPane.showInputDialog("Digita numero 2");
        int num2=Integer.parseInt(ventNum2);
        int rr=num1-num2;
        JOptionPane.showMessageDialog(null, "La resta es: \n "+rr);
    }
    public void divi(){
        String ventNum1=JOptionPane.showInputDialog("Digita numero 1");
        int num1=Integer.parseInt(ventNum1);
        String ventNum2=JOptionPane.showInputDialog("Digita numero 2");
        int num2=Integer.parseInt(ventNum2);
        int rd=num1/num2;
        JOptionPane.showMessageDialog(null, "La division es: \n "+rd);
    }
    public void multi(){
        String ventNum1=JOptionPane.showInputDialog("Digita numero 1");
        int num1=Integer.parseInt(ventNum1);
        String ventNum2=JOptionPane.showInputDialog("Digita numero 2");
        int num2=Integer.parseInt(ventNum2);
        int rm=num1*num2;
        JOptionPane.showMessageDialog(null, "La multiplicacion es: \n "+rm);
    }
    
}
